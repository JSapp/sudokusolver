# README #

* Uses graph searching to solve sudoku

### How To Use ###

To solve a given sudoku

```
java Solver puzzle.csv
```

Where puzzle.csv is a comma-separated values file containing 9 lines of 9 digits

### Solving Method ###

Each cell of a sudoku is constrained by the cells around it. The most constrained cells have the smallest
possible value sets, and are the easiest to figure out. This program assigns potential values to cells
starting with the most constrained cells to reduce work, and reloads to a potentially correct puzzle state
upon reaching an invalid configuration. It is essentially a best-first search with backtracking. Although
it involves some brute forcing, it still has a decent runtime.