
/**
 * Class to represent a block of cells in sudoku
 * @author James Sapp
 *
 */
public class Block {

	private int[][] cells = new int[3][3];
	
	/**
	 * Constructor
	 */
	public Block()
	{
		for(int i = 0; i < cells.length; i++)
			for(int j = 0; j < cells[0].length; j++)
				cells[i][j] = 0;
	} // Block
	
	/**
	 * Copy constructor
	 * @param block Block to copy
	 */
	public Block(Block block)
	{
		for(int i = 0; i < cells.length; i++)
			for(int j = 0; j < cells[0].length; j++)
				this.cells[i][j] = block.cells[i][j];
	} // Block
	
	/**
	 * Assigns a number to a cell in block
	 * @param col Column of cell
	 * @param row Row of cell
	 * @param x Number to assign
	 */
	public void assign(int row, int col, int x)
	{
		cells[row][col] = x;
	} // assign
	
	/**
	 * Check if block has a number
	 * @param x Number to check for
	 * @return True if found, false if not
	 */
	public boolean hasNumber(int x)
	{
		boolean found = false;
		
		for(int i = 0; i < cells.length; i++)
			for(int j = 0; j < cells[0].length; j++)
				if(cells[i][j] == x)
					found = true;
		
		return found;
	} // hasNumber
	
} // Block
