
/**
 * Class to represent a number cell in sudoku
 * @author James Sapp
 *
 */
public class Cell {

	private boolean[] possibleValues = new boolean[]{ true, true, true, true, true, true, true, true, true };
	private int row, col;
	private int blockRow = 0, blockCol = 0;
	private int potentialValue = 0;
	
	/**
	 * Constructor
	 * @param row Row of cell
	 * @param col Column of cell
	 * @param x Number to assign (1 - 9)
	 */
	public Cell(int row, int col, int x)
	{
		assign(x);
		
		this.row = row;
		this.col = col;
		
		/* Determine which row this cell's block is in */
		if(row > 2)
			blockRow = 1;
		if(row > 5)
			blockRow = 2;
		
		/* Determine which column this cell's block is in */
		if(col > 2)
			blockCol = 1;
		if(col > 5)
			blockCol = 2;
	} // Cell
	
	/**
	 * Copy constructor
	 * @param cell Cell to copy
	 */
	public Cell(Cell cell)
	{
		this.row = cell.row;
		this.col = cell.col;
		this.blockRow = cell.blockRow;
		this.blockCol = cell.blockCol;
		this.potentialValue = cell.potentialValue;
		
		for(int i = 0; i < possibleValues.length; i++)
			this.possibleValues[i] = cell.possibleValues[i];
	} // Cell
	
	/**
	 * Assigns a potential number to this cell (1 - 9)
	 * @param x Potential number to assign
	 */
	public void assign(int x)
	{
		if(x > 0 && x < 10)
			potentialValue = x;
	} // assign
	
	/**
	 * Returns column index of this cell's block
	 * @return column index of this cell's block
	 */
	public int getBlockCol()
	{	
		return blockCol;
	} // getBlockCol
	
	/**
	 * Returns row index of this cell's block
	 * @return Row index of this cell's block
	 */
	public int getBlockRow()
	{	
		return blockRow;
	} // getBlockX
	
	/**
	 * Returns the column index of this cell
	 * @return The column index of this cell
	 */
	public int getCol()
	{
		return col;
	} // getCol
	/**
	 * Returns the number of possible values for this cell
	 * @return The number of possible values for this cell
	 */
	public int getConstraints()
	{
		int count = 0;
		
		for(int i = 0; i < possibleValues.length; i++)
			if(possibleValues[i] == true)
				count++;
				
		return count;
	} // getConstraints
	
	/**
	 * Returns next candidate for cell
	 * @return Next candidate for cell
	 */
	public int getNext()
	{
		for(int i = 0; i < possibleValues.length; i++)
			if(possibleValues[i] == true)
				return i + 1;
		
		return -1;
	} // getNext
	
	/**
	 * Returns the number for this cell, or 0 if number if undecided
	 * @return The number for this cell
	 */
	public int getNumber()
	{
		int number = 0;

		if(potentialValue != 0 && possibleValues[potentialValue - 1] == true)
			number = potentialValue;

		return number;
	} // getNumber
	
	/**
	 * Returns the row index of this cell
	 * @return The row index of this cell
	 */
	public int getRow()
	{
		return row;
	} // getRow
	
	/**
	 * Removes a number from consideration
	 * @param x Number to remove
	 */
	public void setFalse(int x)
	{
		if(x > 0 && x < 10)
			possibleValues[x - 1] = false;
	} // setFalse
} // Cell
