
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.EmptyStackException;
import java.util.Scanner;
import java.util.Stack;

/**
 * Class to solve sudoku puzzles
 * @author James Sapp
 *
 */
public class Solver {
	
	private static int[][] puzzle = new int[9][9];
	
	private static Cell[][] cellArray = new Cell[9][9];
	private static Block[][] blockArray = new Block[3][3];
	private static Stack<Cell[][]> cellStack;
	private static Stack<Block[][]> blockStack;
	private static Stack<Cell> criticalStack;

	private static boolean solved;
	
	private static Scanner in = new Scanner(System.in);
	
	/**
	 * Tries to assign a number to a cell
	 * @param cell Cell to examine
	 * @param x Number to assign
	 * @return True for success, false for failure
	 */
	private static boolean checkConstraints(Cell cell, int x)
	{	
		/* Check rows */
		for(int j = 0; j < cellArray[cell.getRow()].length; j++)
			if(cellArray[cell.getRow()][j].getNumber() == x && j != cell.getCol())
				return false;
		
		/* Check columns */
		for(int i = 0; i < cellArray.length; i++)
			if(cellArray[i][cell.getCol()].getNumber() == x && i != cell.getRow())
				return false;
		
		/* Check block */
		if(blockArray[cell.getBlockRow()][cell.getBlockCol()].hasNumber(x))
			return false;
		
		return true;
	} // checkConstraints
	
	/**
	 * Checks for a solved state
	 * @return True for success, false for fail
	 */
	private static boolean checkSolved()
	{
		solved = true;
		
		/* Loop through cells */
		for(int i = 0; i < cellArray.length; i++)
			for(int j = 0; j < cellArray[0].length; j++)
				if(cellArray[i][j].getNumber() == 0)
					solved = false;
		
		return solved;
	} // checkSolved
	
	/**
	 * Returns the most constrained cell
	 * @return The most constrained cell
	 */
	private static Cell getNext()
	{
		int minConstraints = 10;
		Cell nextCell = null;
		
		for(int i = 0; i < cellArray.length; i++)
			for(int j = 0; j < cellArray[i].length; j++)
				if(cellArray[i][j].getNumber() == 0 && cellArray[i][j].getConstraints() < minConstraints)
				{
					nextCell = cellArray[i][j];
					minConstraints = nextCell.getConstraints();
				} // if
		
		return nextCell;
	} // getNext
	
	/**
	 * Initializes the puzzle
	 */
	private static void initialize()
	{	
		/* Initialize/reset variables */
		blockStack = new Stack<Block[][]>();
		cellStack = new Stack<Cell[][]>();
		criticalStack = new Stack<Cell>();
		solved = false;
				
		/* Fill cell graph with puzzle info */
		for(int i = 0; i < cellArray.length; i++)
			for(int j = 0; j < cellArray[i].length; j++)
				cellArray[i][j] = new Cell(i, j, puzzle[i][j]);
		
		/* Initialize block array with ridiculous loops */
		for(int i = 0; i < blockArray.length; i++)
			for(int j = 0; j < blockArray.length; j++)
			{
				blockArray[i][j] = new Block();
				for(int k = i * 3; k < i * 3 + 3; k++)
					for(int l = j * 3; l < j * 3 + 3; l++)
						blockArray[i][j].assign(k % 3, l % 3, puzzle[k][l]);
			} // for
		
		/* Assign Initial cell constraints */
		for(int i = 0; i < cellArray.length; i++)
			for(int j = 0; j < cellArray[i].length; j++)
				for(int k = 1; k < 10; k++)
					if(cellArray[i][j].getNumber() == 0 && !checkConstraints(cellArray[i][j], k))
						cellArray[i][j].setFalse(k);
	} // initialize
	
	/**
	 * Allows user to choose multiple puzzles
	 */
	private static void inputLoop()
	{	
		boolean running = true;
		
		// Originally there were multiple puzzles to solve, thus a loop
		// Left it here because the code structure makes more senses this way, even if it does nothing
		while(running)
		{
			initialize();
			System.out.println("Initial Puzzle:\n");
			print();
			solve();
			System.out.println("Completed Puzzle:\n");
			print();
			/*
			System.out.println("Choose another puzzle? (y/n)");
			if(in.next().contains("n"))
				running = false;
			*/
			running = false;
		} // while
		
		in.close();
	} // inputLoop
	
	/**
	 * Backtrack to a potentially valid state
	 */
	private static void load()
	{
		try {
			/* Revert to previous state */
			cellArray = cellStack.pop();
			blockArray = blockStack.pop();
			Cell criticalCell = criticalStack.pop(); // Critical cell is current cell being examined
			
			/* Reject last trial value */
			cellArray[criticalCell.getRow()][criticalCell.getCol()].setFalse(criticalCell.getNext());
		} catch(EmptyStackException e) {
			solved = true;
			System.out.println("Puzzle has no solution\n");
		} // catch
	} // load
	
	/**
	 * Displays progress on puzzle solution
	 */
	private static void print()
	{
		for(int i = 0; i < cellArray.length; i++)
			for(int j = 0; j < cellArray[0].length; j++)
				if(j == cellArray[0].length - 1)
					if((i + 1) % 3 == 0 && i != cellArray.length - 1)
						System.out.println(cellArray[i][j].getNumber() + "\n---------------");
					else
						System.out.println(cellArray[i][j].getNumber());
				else
					if((j + 1) % 3 == 0)
						System.out.print(cellArray[i][j].getNumber() + " | ");
					else
						System.out.print(cellArray[i][j].getNumber());
		System.out.println();
	} // print
	
	/**
	 * Try to fill in a cell
	 * @param cell Cell to process
	 * @return True for success, false for fail
	 */
	private static boolean process(Cell cell)
	{
		/* Try all possible values for a cell */
		while(cell.getNext() != -1)
		{
			/* Trial value is found */
			if(checkConstraints(cell, cell.getNext()))
			{
				/* Fill cell and create backtrack branch */
				cell.assign(cell.getNext());
				blockArray[cell.getBlockRow()][cell.getBlockCol()]
						.assign(cell.getRow() % 3, cell.getCol() % 3, cell.getNext());
				save(cell);
				checkSolved();
				return true;
			} // if
			else
			{	
				/* Remove last trial value from potential options */ 
				cell.setFalse(cell.getNext());
			
				/* Backtrack if cell can't be filled */
				if(cell.getNext() == -1)
				{
					load();
					return false;
				} // if
			} // else
		} // while
	
		/* Cell immediately fails after backtracking */
		load();
		return false;
	} // process
	
	/**
	 * Pushes solve state on to stack
	 */
	private static void save(Cell cell)
	{
		/* Create copies */
		Cell[][] cellCopy = new Cell[9][9];
		Block[][] blockCopy = new Block[3][3];
		Cell criticalCell = new Cell(cell); // Critical cell is current cell being examined
		
		/* Copy cells and blocks */
		for(int i = 0; i < cellArray.length; i++)
			for(int j = 0; j < cellArray[0].length; j++)
				cellCopy[i][j] = new Cell(cellArray[i][j]);
		
		for(int i = 0; i < blockArray.length; i++)
			for(int j = 0; j < blockArray[0].length; j++)
				blockCopy[i][j] = new Block(blockArray[i][j]);
		
		/* Push copies to stack */
		cellStack.push(cellCopy);
		blockStack.push(blockCopy);
		criticalStack.push(criticalCell);
	} // save
	
	/**
	 * Primary loop for solving the puzzle
	 */
	private static void solve()
	{
		long start = System.nanoTime();
		
		/* Loop until puzzle is solved */
		while(!solved)
			process(getNext());
		
		long end = System.nanoTime();
		System.out.println("Time: " + (end - start)/1000 + " ns\n");
			
	} // solve
	
	// Maybe split this bad boy up some more, maybe not
	public static void main(String[] args)
	{
		if(args.length == 1)
		{
			try {
				Scanner fileInput = new Scanner(new File(args[0]));
				String puzCSV = "";
				
				while(fileInput.hasNext())
					puzCSV += fileInput.nextLine() + ",";
							
				String[] puzNums = puzCSV.split(",");
				
				if(puzNums.length == 81)
				{
					int k = 0;
					for(int i = 0; i < puzzle.length; i++)
						for(int j = 0; j < puzzle[0].length; j++)
							puzzle[i][j] = Integer.parseInt(puzNums[k++]);
					
					inputLoop();
				} // if
				else
					System.out.println("CSV file must contain nine lines with nine separated numbers on each");
				
				fileInput.close();
			} catch(FileNotFoundException e) {
				System.out.println("Failed to locate file " + args[0]);
			} // catch
		} // if
		else
			System.out.println("Please specify an input csv file");
	
	} // main
	
} // Solver
